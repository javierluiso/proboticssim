#import matplotlib
#matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.patches as ppatches
import math
import numpy as np

import sys
import json

def drawRobot(ax, x,y,theta, robot_diameter, robot_color):
    rradius = robot_diameter / 2
    circle = plt.Circle((x,y), radius=rradius, fill=False, color=robot_color)
    ax.add_artist(circle)
    bearing = plt.Line2D([x, x + rradius * np.cos(theta)], [y, y + rradius * np.sin(theta)], linewidth=2, color=robot_color)
    ax.add_line(bearing)
    ax.axis('equal')
    ax.autoscale()
    return (circle, bearing)

def drawRobotPos(ax, pos, robot_diameter, robot_color):
    x = pos[0]
    y = pos[1]
    theta = pos[2]
    rradius = robot_diameter / 2
    circle = plt.Circle((x,y), radius=rradius, fill=False, color=robot_color)
    ax.add_artist(circle)
    bearing = plt.Line2D([x, x + rradius * np.cos(theta)], [y, y + rradius * np.sin(theta)], linewidth=2, color=robot_color)
    ax.add_line(bearing)
    ax.axis('equal')
    ax.autoscale()
    return (circle, bearing)

def drawOccMapBorder(ax, occ_map_width, occ_map_height):
    rect = ppatches.Rectangle((-occ_map_width / 2.0, -occ_map_height / 2.0), occ_map_width, occ_map_height, edgecolor = 'blue', facecolor = "none")
    ax.add_patch(rect)

def drawOccMap(ax, occ_map, occ_map_width, occ_map_height):
    x_step = occ_map_width / occ_map.shape[1]
    y_step = occ_map_height / occ_map.shape[0]

    for r in range (0, occ_map.shape[0], 1):
        for c in range (0, occ_map.shape[1], 1):
            if occ_map[r, c] < 0.5 :
                rect = ppatches.Rectangle((-occ_map_width / 2.0 + c * x_step, occ_map_height / 2.0 - (r + 1) * y_step), x_step, y_step, edgecolor = 'None', facecolor = "yellow")
                ax.add_patch(rect)


def drawLidarMeasure(ax, pos, measures_points, measures_values):
    x = pos[0]
    y = pos[1]
    theta = pos[2]
    dtheta = 2 * math.pi / measures_points
    lidar_angle = 0
    points_x = np.zeros(measures_points)
    points_y = np.zeros(measures_points)

    for i in range(0, measures_points):
        dx = measures_values[i] * math.cos(theta - math.pi + lidar_angle)
        dy = measures_values[i] * math.sin(theta - math.pi + lidar_angle)
        points_x[i] = x + dx
        points_y[i] = y + dy
        lidar_angle += dtheta

    lidarPoints = ax.scatter(points_x, points_y, c = 'r',marker = '.')

    return lidarPoints

def updatedLidarDraw(lidarPoints, pos, new_data_values, lidar_resolution, map_center, map_scale):
    x = pos[0] * map_scale[0] + map_center[0]
    y = pos[1] * map_scale[1] + map_center[1]
    theta = pos[2]
    dtheta = 2 * math.pi / lidar_resolution
    lidar_angle = 0
    pts_x = np.zeros(lidar_resolution)
    pts_y = np.zeros(lidar_resolution)
    for i in range(0, lidar_resolution):
        dx = new_data_values[i] * math.cos(theta - math.pi + lidar_angle)
        dy = new_data_values[i] * math.sin(theta - math.pi + lidar_angle)
        pts_x[i] = x + dx * map_scale[0]
        pts_y[i] = y + dy * map_scale[1]
        lidar_angle += dtheta

    lidarPoints.set_offsets(np.c_[pts_x, pts_y])
    


args_nr = len(sys.argv)

if args_nr < 2:
    print('ERROR - Please indicate record.json file to draw')
    record_file = 'run01.json'
else:
    record_file = sys.argv[1]

with open(record_file) as json_file:
    record_data = json.load(json_file)

first_status_record = 0
occ_map_width = 0 
occ_map_height = 0

if record_data[0][0] == 'OCC_MAP':
    occupation_map = np.array(record_data[0][2])
    occ_map_height = occupation_map.shape[0] * record_data[0][1]
    occ_map_width = occupation_map.shape[1] * record_data[0][1]
    first_status_record +=1

# Robot Data
robot_diameter = 0.35
lidar_resolution = 144
lidar_values = np.zeros(lidar_resolution)

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.pcolormesh(np.flipud(occupation_map))
ax.axis('off')

map_center = (occupation_map.shape[0] /2.0, occupation_map.shape[1] / 2.0)
map_scale = (occupation_map.shape[0] / occ_map_width, occupation_map.shape[1] / occ_map_height)

plt.ion()
(gt_icon, gt_bearing) = drawRobot(ax, 0 + map_center[0], 0 + map_center[1], 0, robot_diameter * map_scale[0], 'g')
(od_icon, od_bearing) = drawRobot(ax, 0 + map_center[0], 0 + map_center[1], 0, robot_diameter * map_scale[0], 'grey')
plt.draw()
plt.pause(1)

lidarPoints = drawLidarMeasure(ax, [0 ,0 ,0], lidar_resolution, lidar_values)

status_text = ax.text(0, -1.5, "")

records_entries = len(record_data)

for i in range(first_status_record, records_entries):

    status = record_data[i]
    gt = status[4]
    od = status[5]

    # gt_icon.set_center((gt[0] * map_scale[0] + map_center[0], gt[1] * map_scale[1] + map_center[1]))  # only for Ptyhon 3
    gt_icon.center = gt[0] * map_scale[0] + map_center[0], gt[1] * map_scale[1] + map_center[1]
    gt_bearing.set_xdata([gt[0] * map_scale[0] + map_center[0], gt[0] * map_scale[0] + map_center[0] + 0.5 * map_scale[0] * robot_diameter * np.cos(gt[2])])
    gt_bearing.set_ydata([gt[1] * map_scale[1] + map_center[1], gt[1] * map_scale[1] + map_center[1] + 0.5 * map_scale[1] * robot_diameter * np.sin(gt[2])])

    # od_icon.set_center((od[0]* map_scale[0]  + map_center[0], od[1] * map_scale[1]  + map_center[1]))  # only for Ptyhon 3
    od_icon.center = od[0]* map_scale[0]  + map_center[0], od[1] * map_scale[1]  + map_center[1]
    od_bearing.set_xdata([od[0] * map_scale[0] + map_center[0], od[0] * map_scale[0] + map_center[0] + 0.5 * robot_diameter * map_scale[0] * np.cos(od[2])])
    od_bearing.set_ydata([od[1] * map_scale[1] + map_center[1], od[1] * map_scale[1] + map_center[1] + 0.5 * robot_diameter * map_scale[1] * np.sin(od[2])])

    
    if status[10] == True:
        updatedLidarDraw(lidarPoints, od, status[11], len(status[11]), map_center, map_scale)

    s_text = "time = " + "{:.2f}".format(status[1] * status[2]) + "seg   u_t=(" + "{:.2f}".format(status[3][0])  + " ; " + "{:.2f}".format(status[3][1]) + ") Collision = " + str(status[8]) 
    status_text.set_text(s_text)

    plt.draw()
    plt.pause(status[2])




