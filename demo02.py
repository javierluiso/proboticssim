
from diffRobot import DiffRobot

import numpy as np
import math
import time
import timeit
import functools
import sys

# ##################################################################################3  

args_nr = len(sys.argv)

if args_nr < 4:
    print('ERROR - Please indicate occupation map file, scale factor and json filename')
    print('example: python demo02.py my_occ_map.png 0.xx my_json_file.json')
    print('Demo will use by default occ_map_01.png with scale factor of 0.5 m / px and run.json')
    occupation_map_file = 'occ_map_01.png'
    occupation_map_scale_factor = 0.5
    json_filename = 'run.json'
else:
    occupation_map_file = sys.argv[1]
    occupation_map_scale_factor = float(sys.argv[2])
    json_filename = sys.argv[3]


# 1. Create an instance of DiffRobot
my_robot = DiffRobot()

# 2. Load occupation Map
# Use a grey image as an occupation map. Values > 128 will be considered occupied.
# Indicate de resolution desired for the map as a meter / px value
my_robot.loadOccupationMap(occupation_map_file, occupation_map_scale_factor)

##############################################################################################
# Set simulation parameters
my_robot.dt = 0.1
# Start simulation thread
# You can indicate console print robot's status
my_robot.startSimulation(print_status=True)

################################################################################################
# Auxiliary variables for this demo
simulation_steps = 500

#################################################################################################
# This demo implements a very simple robot control algorithm
# 1. Robot is put in motion with constant linear and angular velocity: u = (0.85, 0.35) 
# 2. If a collision is detected angular velocity is set to 0, and linear velocity is set to a 
#    negative value for 5 iteration of control algorithm. The robot will move away for obstacle.
# 3. Then, linear velocity is set to 0 and angular velocity is set in order to rotate the robot in place.
#    for 5 iteration of control algorithm.
# 4. Then robot is put in motion again a step 1.
#  
# Bumper Sensor is enabled in first control algorithm iteration
# Lidar sensor is turn ON in first control algorithm iteration
#
# In this demo the status of robot is internally recorder and after simulation
# the status records are write down in a JSON file for later analisys.
# you can use drawStatusRecord.py script to draw the simulation
#
# Modeling robot status
# 1: moving free
# 2: getting away form obstacle
# 3: turning
robot_status = 1
aux_counter = 0

for i in range(simulation_steps):
    if i == 0:
        my_robot.setLinearVelocity(0.85)
        my_robot.setAngularVelocity(0.35)
        my_robot.enableBumper()
        my_robot.turnLidar_ON()
        my_robot.startRecordStatus()
    else:
        pass

    if robot_status == 1:
        if my_robot.getBumperSensorValue() == True:
            my_robot.setLinearVelocity(-2.00)
            my_robot.setAngularVelocity(0)
            robot_status = 2
            aux_counter = 5
        else:
            my_robot.setLinearVelocity(0.85)
            my_robot.setAngularVelocity(0.35)
    elif robot_status == 2:
        if aux_counter > 0:
            my_robot.setLinearVelocity(-2.00)
            my_robot.setAngularVelocity(0)
            aux_counter -= 1
        else:
            robot_status = 3
            aux_counter = 5
    elif robot_status == 3:
        if aux_counter > 0:
            my_robot.setLinearVelocity(0)
            my_robot.setAngularVelocity(0.50)
            aux_counter -= 1
        else:
            robot_status = 1
    else:
        pass

    time.sleep(my_robot.dt)
    
my_robot.stopRecordStatus()
my_robot.pauseSimulation()
my_robot.saveRecordStatus(json_filename)

 
my_robot.stopSimulation()
