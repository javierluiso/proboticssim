# pRoboticsSim

pRoboticsSim es un simulador de un robot diferencial.

Este simulador ha sido desarrollado como herramienta para el curso ["Robótica Móvil - un enfoque probabilístico"](https://campus.fi.uba.ar/enrol/index.php?id=2421) de la Facultad de Ingeniería, Universidad de Buenos Aires.


## Versión 1.0.0
Release date: 08-Ago-2020

## Características del robot

El robot tiene una forma cilíndrica de 0.35 m de diámetro y 0.10 m de altura. La separacion entre ruedas de 0.235 m y el diámetro de ruedas esde 0.072 m.

### Sensores

El robot cuenta con un sensor *bumper* ubicado en el frente del robot. Este sensor se puede habilitar o deshabilitar.

![Sensor Bumper](img/bumper.png)

Además el robot está equipado con un sensor LIDAR. El simulador trabaja con una resolución de 144 puntos. 

![Sensor LIDAR](img/lidar.png)

## Modelo del robot

El robot acepta como comando de entrada una valor de velocidad lineal y velocidad angular.
```math
u_t = (v_t, w_t)
```
con:

- $`v_t`$: velocidad lineal expresada en $`\dfrac{m}{seg}`$
- $`w_t`$: velocidad angular expresada en $`\dfrac{rad}{seg}`$

El modelo probabilistico implementado en el simulador se corresponde con el descripto en el capítulo 5.3 *Velocity Motion Model* del libro Probabilistic Robots de Sebastian Thrun.

Se utiliza como algoritmo para obtener una muestra de una distribución normal de medio nula y desvío dado el algoritmo detallado en la Table 5.4

El vector de parámetros de perturbacíon por default adopta los siguientes valores. Ver 5.3.2 *Sampling algorithm*

```math
\alpha = (0.10, 0.20, 0.10, 0.20, 0.25, 0.25)
```
#### Modelo del sensor LIDAR
El sensor LIDAR se modela con una resolución de 144 mediciones, una lectura cada 2.5 grados. Para modelar el error en la medición se utiliza una distribución normal con:
- $`\mu = s`$
- $`\sigma = 0.15 * s`$

siendo $`s`$ la lectura del sensor.

Para el cálculo del valor de $`s`$ se utiliza un algoritmo de ray-tracing con un paso $`\delta = \dfrac{1}{8} * r_{OM}`$, siendo $`r_{OM}`$ la resolución del mapa de ocupación.

#### Modelo de colisión

La colisión tiene sentido cuando se encuentra cargado un mapa de ocupación. El mapa define una superficie donde la simulación se realiza y las posiciones ocupadas dentro de dicho espacio. El robot colisiona con los bordes de la superficie como con las posiciones ocupadas.

En función de si el sensor de *bumper* se encuentra habilitado o no, el robot de comportará de diferente maneras ante una colisión. 
Este comportamiento tiene sentido únicamente si se encuentra cargado un mapa de ocupación.

- Sensor de bumper habilidato:

Estando habilitado este sensor, si el robot colisiona avanzando hacia adelante, el sensor se activará y mientras se encuentre activado solamente aceptará comandos de entrada con valores negativos de velocidad lineal. Es decir que espera un comando que lo haga retroceder. Cualquier valor $`v_t > 0`$ será ignorado. 

En este estado, el robot acepta cualquier valor de velocidad angular. Es decir que estando colisionado puede girar.

Si el robot colisiona avanzando hacia atrás, el sensor de bumper nunca se activa. El comportamiento del robot es similar al que tiene cuando el sensor está deshablitado.

- Sensor de bumper deshabilitado

Estando deshabilitado el sensor, si el robot colisiona, sus ruedas continuan girando sin rozamiento. Es decir que resbalan sobre el suelo.

#### Mapa de ocupación

El simulador permite cargar un mapa de ocupación en formato de imagen PNG. La imagen deberá ser de 8bpp (escala de grises). Al momento de cargar el mapa se deberá indicar la resolución del mismo en valor de px / m (pixel por metro).

Los pixels cuyo valor sea superior a *128* se consideran no ocupados.

## Interfaz con el robot


- Para crear una instancia del robot es necesario importar el archivo diffRobot.py y definir una variable del tipo *DiffRobot*

```python
from diffRobot import DiffRobot

# Create an instance of DiffRobot
my_robot = DiffRobot()

#### Cargar un mapa de ocupación
```
Para cargar un mapa de ocupación:
```python
# Load occupation Map
# Use a grey image as an occupation map. Values < 128 will be considered occupied.
# Indicate de resolution desired for the map as a meter / px value
my_robot.loadOccupationMap(occupation_map_file, occupation_map_scale_factor)

```
Una vez cargado el mapa se puede consultar el mapa. El formato devuelto es una matríz con valores en el rango de $`[0.0, 1.0]`$ (cualquier valor inferior a 0.5 se considera ocupado0).

También se pueden consultar las dimensiones del mapa en coordenadas del mundo, es decir en metros.

```python
# We can ask the size of occupation map in world coordinates and get 
# the map as a matrix for drawing purpose por example.
(occ_map_height, occ_map_width) = my_robot.getOccMapSizeInWorld()

occ_map_matrix = my_robot.getOccMap()

```
#### Parámetros de la simulación
Los siguientes parámetros físicos del robot se pueden acceder directamente si se desea modificar su valor por defecto

```python


# Kinematics parameters 
# values expressed in meters

# Wheel's radio
my_robot.r = 0.072 
# Wheel's separation
my_robot.l = 0.235

# Robot dimensions
# Robot shape = cylinder
# values expressed in meters
my_robot.height = 0.10
my_robot.diameter = 0.35

```

La constante de tiempo que define el paso de simulación y los errores usados en el modelo probabilísticos también pueden modificarse directamente

```python
# Step Simulation [seg]
my_robot.dt = 1.0

#Error parameters
my_robot.alpha = (0.10, 0.20, 0.10, 0.20, 0.25, 0.25)
```

#### Control de la simulación
Cada instancia de la clase DiffRobot puede correr una simulación independiente. El control de la misma se realiza mediante los siguientes métodos.

```python
# Start the simulation.
# The optional parameter controls de console print out of simulation status
my_robot.startSimulation(print_status = True)

# To pause simulation
my_robot.pauseSimulation()

# To resume simulation
my_robot.continueSimulation()

# To stop simulation
my_robot.stopSimulation()
```

En cualquier momento es posible consultar el estado de avance de la simulacion
```python
# get simulation step counter
my_robot.getSimStepCounter()

# get simulation time elapsed
my_robot.getSimulationTime()
```
El control del robot se realiza utilizando los siguientes métodos que permiten indicar las velocidades de entrada, lineal y angular:
```python
my_robot.setLinearVelocity(v)

my_robot.setAngularVelocity(w)

my_robot.setInput(v,w)
```

#### Consultar el estado del robot
En cualquier momento es posible consultar el estado del robot:
- Ground Truth
- Odometry
- Linear Velocity
- Angular Velocity
```python
# devuelve una ntupla
# (x, y, theta)
my_robot.getGroundTruth()

# devuelve una ntupla
# (x, y, theta)
my_robot.getOdometry()

my_robot.getLinearVelocity()

my_robot.getAngularVelocity()
```

#### Control de los sensores
##### Sensor Bumper
El sensor puede activarse y/o desactivarse. Su estado de activación se puede consultar como también el valor del sensor. 
```python
my_robot.enableBumper()

my_robot.disableBumper()

my_robot.isBumperEnable()

my_robot.getBumperSensorValue()
```
##### Sensor LIDAR
El sensor puede activarse y/o desactivarse. Su estado de activación se puede consultar como también el valor de la última medición efectuada.

El sensor trabaja realizando una medición en cada paso de simulación.
```python
# Lidar Control Interface
my_robot.turnLidar_ON)

my_robot.turnLidar_OFF()

# devuelve un numpy array
my_robot.getLidarMeaurement()

my_robot.getLidarResolution()
```
#### Registro de la simulación
El simulador permite realizar un registro de la simulación para luego guardarlo en un archivo JSON.

En cualquier momento se puede dar inicio al registro y en cada paso de la simulación el estado del robot, los valores de entrada (velocidades) y la medición del LIDAR se guardarán.

Una vez detenido el registro, se puede guardar. Si el registro vuelve a activarse, se borra cualquier registro anterior que pudiera haberse realizado.
```python
my_robot.startRecordStatus()

my_robot.stopRecordStatus()

# Si el registro está activado, no tiene efecto la llamada
# a esta función.
# Para poder guardar el registro en un archivo JSON primero debe
# detenerse el regsitro.
#
# Indicar nombre de archivo con extensión .json incluida
my_robot.saveRecordStatus(filename)
```
Los registros guardados pueden visualizarse utilizando el script *drawStatusRecord.py*

El desarrollo de la simulación es mostrado en tiempo real con el siguiente formato
![](img/drawStatusRecordExample.png)

Donde se puede observar:
- El mapa de ocupación
- La medición del sensor LIDAR (rojo)
- La posición GroundTruth del robot (ícono en verde)
- La posición Odometry del robot (ícono en gris)

#### Control externo de la simulacíon
La desarrollo de la simulación de una instancia de DiffRobot puede controlarse externamente sin hacer uso de la simulación interna.

Para esto es necesario no llamar al método
```python
# Start the simulation.
# The optional parameter controls de console print out of simulation status
my_robot.startSimulation(print_status = True)
```
En su lugar se utilizará el método
```python
my_robot.foward(v,w)
```
Cada llamada a este método implica avanzar un paso en la simulación usando el *dt* del robot.

De este modo se puede ensayar el robot trabajando en tiempo *no* real.

## Ejemplos
Se adjuntan dos ejemplos de uso del simulador en los scripts
- demo01.py mapa_de_ocupacion.png res_px_m
- demo02.py mapa_de_ocupacion.png res_px_m nombre_archivo.json

En ambos se implementa un sencillo algoritmo de control en donde si el robot colisiona, durante 3 ciclos de simulación el robot simplemente retrocede para luego detenerse y girar. Completado el giro vuelva a intentar continuar avanzando como lo venía haciendo antes de la colisión.
Si vuelve a colisionar porque el giro no fue suficiente, repite todo el proceso.

En demo01.py se grafica el estado de la simulación. Hay que tener presente que el refresco del gráfico se realiza de manera asincrónica con el algoritmo que modela la simulación del robot. Dependiendo de la capacidad de la plataforma donde se corra el demo, puede ocurrir que el gráfico se actualize a intervalos mayores al *dt* fijado para el ciclo de simulación. Como ayuda se imprime en el gráfico el tiempo de simulación correspondiente.
![](img/demo01.png)

En demo02.py no se grafica el estado de la simulación. En su lugar se hace un registro y se guarda en el archivo JSON indicado.

### Estrucutra del archivo JSON
El archivo JSON guarda el mapa de ocupación y el estado de cada paso de simulación.
El primer elemento es un array:
- OCC_MAP
- resolución px / m
- matriz del mapa de ocupación

Los elememntos siguientes son arrays que representan el estado de la simulación:
- SIM_STEP
- step counter
- dt
- input (v,w)
- ground truth (x, y, theta)
- odometry (x, y, theta)
- Bumper sensor enable (True / False)
- Bumper Sensor value (True, False)
- Colisión detectada (True, False)
- Borde del escenario alcanzado (True, False)
- LIDAR sensor enable (True / False)
- Meidición del sensor LIDAR (solamente si está activado)


