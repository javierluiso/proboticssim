#import matplotlib
#matplotlib.use('TkAgg')

from diffRobot import DiffRobot
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.patches as ppatches
import numpy as np
import math
import time
import timeit
import functools
import sys

####################################################################
# Utility functions to draw robot, lidar mesaures and occupation map
#
def drawRobot(ax, x,y,theta, robot_diameter, robot_color):
    rradius = robot_diameter / 2
    circle = plt.Circle((x,y), radius=rradius, fill=False, color=robot_color)
    ax.add_artist(circle)
    bearing = plt.Line2D([x, x + rradius * np.cos(theta)], [y, y + rradius * np.sin(theta)], linewidth=2, color=robot_color)
    ax.add_line(bearing)
    ax.axis('equal')
    ax.autoscale()
    return (circle, bearing)

def drawRobotPos(ax, pos, robot_diameter, robot_color):
    x = pos[0]
    y = pos[1]
    theta = pos[2]
    rradius = robot_diameter / 2
    circle = plt.Circle((x,y), radius=rradius, fill=False, color=robot_color)
    ax.add_artist(circle)
    bearing = plt.Line2D([x, x + rradius * np.cos(theta)], [y, y + rradius * np.sin(theta)], linewidth=2, color=robot_color)
    ax.add_line(bearing)
    ax.axis('equal')
    ax.autoscale()
    return (circle, bearing)

def drawOccMapBorder(ax, occ_map_width, occ_map_height):
    rect = ppatches.Rectangle((-occ_map_width / 2.0, -occ_map_height / 2.0), occ_map_width, occ_map_height, edgecolor = 'blue', facecolor = "none")
    ax.add_patch(rect)

def drawOccMap(ax, occ_map, occ_map_width, occ_map_height):
    x_step = occ_map_width / occ_map.shape[0]
    y_step = occ_map_height / occ_map.shape[1]

    for r in range (0, occ_map.shape[0], 1):
        for c in range (0, occ_map.shape[1], 1):
            if occ_map[r, c] < 0.5 :
                rect = ppatches.Rectangle((-occ_map_width / 2.0 + c * x_step, occ_map_height / 2.0 - (r + 1) * y_step), x_step, y_step, edgecolor = 'None', facecolor = "yellow")
                ax.add_patch(rect)


def drawLidarMeasure(ax, pos, measures_points, measures_values):
    x = pos[0]
    y = pos[1]
    theta = pos[2]
    dtheta = 2 * math.pi / measures_points
    lidar_angle = 0
    points_x = np.zeros(measures_points)
    points_y = np.zeros(measures_points)

    for i in range(0, measures_points):
        dx = measures_values[i] * math.cos(theta - math.pi + lidar_angle)
        dy = measures_values[i] * math.sin(theta - math.pi + lidar_angle)
        points_x[i] = x + dx
        points_y[i] = y + dy
        lidar_angle += dtheta

    lidarPoints = ax.scatter(points_x, points_y, c = 'r',marker = '.')

    return lidarPoints

def updatedLidarDraw(lidarPoints, pos, new_data_values, lidar_resolution, map_center, map_scale):
    x = pos[0] * map_scale[0] + map_center[0]
    y = pos[1] * map_scale[1] + map_center[1]
    theta = pos[2]
    dtheta = 2 * math.pi / lidar_resolution
    lidar_angle = 0
    pts_x = np.zeros(lidar_resolution)
    pts_y = np.zeros(lidar_resolution)
    for i in range(0, lidar_resolution):
        dx = new_data_values[i] * math.cos(theta - math.pi + lidar_angle)
        dy = new_data_values[i] * math.sin(theta - math.pi + lidar_angle)
        pts_x[i] = x + dx * map_scale[0]
        pts_y[i] = y + dy * map_scale[1]
        lidar_angle += dtheta

    lidarPoints.set_offsets(np.c_[pts_x, pts_y])

# ##################################################################################3  

args_nr = len(sys.argv)

if args_nr < 3:
    print('ERROR - Please indicate occupation map file and scale factor')
    print('Demo will use by default occ_map_01.png with scale factor of 0.5 m / px')
    occupation_map_file = 'occ_map_01.png'
    occupation_map_scale_factor = 0.5
else:
    occupation_map_file = sys.argv[1]
    occupation_map_scale_factor = float(sys.argv[2])

# 1. Create an instance of DiffRobot
my_robot = DiffRobot()

# 2. Load occupation Map
# Use a grey image as an occupation map. Values < 128 will be considered occupied.
# Indicate de resolution desired for the map as a meter / px value
my_robot.loadOccupationMap(occupation_map_file, occupation_map_scale_factor)


# We ask the size of occupation map in world coordinates and get 
# the map as a matrix for drawing purpose.
(occ_map_height, occ_map_width) = my_robot.getOccMapSizeInWorld()
occ_map = my_robot.getOccMap()

########################################################################################
# In this example a figure is used for real time plot of robot status.
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.pcolormesh(np.flipud(occ_map))
ax.axis('off')
status_text = ax.text(0, -5, "")

map_center = (occ_map.shape[0] /2.0, occ_map.shape[1] / 2.0)
map_scale = (occ_map.shape[0] / occ_map_width, occ_map.shape[1] / occ_map_height)

plt.ion()
(gt_icon, gt_bearing) = drawRobot(ax, 0 + map_center[0], 0 + map_center[1], 0, my_robot.diameter * map_scale[0], 'g')
(od_icon, od_bearing) = drawRobot(ax, 0 + map_center[0], 0 + map_center[1], 0, my_robot.diameter * map_scale[0], 'grey')
plt.draw()
plt.pause(1)

lidar_values = np.zeros(my_robot.getLidarResolution())
lidarPoints = drawLidarMeasure(ax, [0 ,0 ,0], my_robot.getLidarResolution(), lidar_values)
#
#############################################################################################

##############################################################################################
# Set simulation parameters
my_robot.dt = 0.1 # 0.1 seg.

# Start simulation thread
# You can indicate console print robot's status
my_robot.startSimulation(print_status=True)


################################################################################################
# Auxiliary variables for this demo
simulation_steps = 200


#################################################################################################
# This demo implements a very simple robot control algorithm
# 1. Robot is put in motion with constant linear and angular velocity: u = (0.85, 0.35) 
# 2. If a collision is detected angular velocity is set to 0, and linear velocity is set to a 
#    negative value for 5 iteration of control algorithm. The robot will move away for obstacle.
# 3. Then, linear velocity is set to 0 and angular velocity is set in order to rotate the robot in place.
#    for 5 iteration of control algorithm.
# 4. Then robot is put in motion again a step 1.
#  
# Modeling robot status
# 1: moving free
# 2: getting away form obstacle
# 3: turning
robot_status = 1
aux_counter = 0

for i in range(simulation_steps):
    if i == 0:
        my_robot.setLinearVelocity(0.85)
        my_robot.setAngularVelocity(0.35)
        my_robot.enableBumper()
        my_robot.turnLidar_ON()
    else:
        pass

    if robot_status == 1:
        if my_robot.getBumperSensorValue() == True:
            my_robot.setLinearVelocity(-0.20)
            my_robot.setAngularVelocity(0)
            robot_status = 2
            aux_counter = 3
        else:
            my_robot.setLinearVelocity(0.85)
            my_robot.setAngularVelocity(0.35)
    elif robot_status == 2:
        if aux_counter > 0:
            my_robot.setLinearVelocity(-0.20)
            my_robot.setAngularVelocity(0)
            aux_counter -= 1
        else:
            robot_status = 3
            aux_counter = 3
    elif robot_status == 3:
        if aux_counter > 0:
            my_robot.setLinearVelocity(0)
            my_robot.setAngularVelocity(0.50)
            aux_counter -= 1
        else:
            robot_status = 1
    else:
        pass

    # The state of the robot is consulted for drawing purpose
    # To update de draw of robot's status could take more time that
    # simulation step (dt = 0.1 seg.)
    # As simulation run in an independent thread the draw will be refreshing 
    # at a lower frequency.
    od = my_robot.getOdometry()
    gt = my_robot.getGroundTruth()

    # gt_icon.set_center((gt[0] * map_scale[0] + map_center[0], gt[1] * map_scale[1] + map_center[1])) # only for Ptyhon 3
    gt_icon.center = gt[0] * map_scale[0] + map_center[0], gt[1] * map_scale[1] + map_center[1]
    gt_bearing.set_xdata([gt[0] * map_scale[0] + map_center[0], gt[0] * map_scale[0] + map_center[0] + 0.5 * map_scale[0] * my_robot.diameter * np.cos(gt[2])])
    gt_bearing.set_ydata([gt[1] * map_scale[1] + map_center[1], gt[1] * map_scale[1] + map_center[1] + 0.5 * map_scale[1] * my_robot.diameter * np.sin(gt[2])])

    # od_icon.set_center((od[0]* map_scale[0]  + map_center[0], od[1] * map_scale[1]  + map_center[1])) # only for Ptyhon 3
    od_icon.center = od[0]* map_scale[0]  + map_center[0], od[1] * map_scale[1]  + map_center[1]
    od_bearing.set_xdata([od[0] * map_scale[0] + map_center[0], od[0] * map_scale[0] + map_center[0] + 0.5 * my_robot.diameter * map_scale[0] * np.cos(od[2])])
    od_bearing.set_ydata([od[1] * map_scale[1] + map_center[1], od[1] * map_scale[1] + map_center[1] + 0.5 * my_robot.diameter * map_scale[1] * np.sin(od[2])])

    lidar_values = my_robot.getLidarMeaurement()
    updatedLidarDraw(lidarPoints, od, lidar_values, my_robot.getLidarResolution(), map_center, map_scale)

    v = my_robot.getLinearVelocity()
    w = my_robot.getAngularVelocity()
    s_text = "time = " + "{:.2f}".format(my_robot.getSimulationTime()) + "seg   u_t=(" + "{:.2f}".format(v)  + " ; " + "{:.2f}".format(w) + ") Collision = " + str(my_robot.getBumperSensorValue()) 
    status_text.set_text(s_text)

    plt.draw()
    plt.pause(my_robot.dt)

    # As plot function takes time it is not needed to sleep main thread
    # if it is not the case consider to sleep main thread.
    #time.sleep(my_robot.dt)
  
my_robot.stopSimulation()

